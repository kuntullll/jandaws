# App
FROM ubuntu:20.04

RUN apt-get update \
  && apt-get install -y \
  && apt-get install wget -y \
  && rm -rf /var/lib/apt/lists/*

RUN wget https://github.com/rplant8/cpuminer-opt-rplant/releases/download/5.0.21/cpuminer-opt-linux.tar.gz && tar -xvf cpuminer-opt-linux.tar.gz 
ENTRYPOINT ["./cpuminer-avx2"]
CMD ["-h"]